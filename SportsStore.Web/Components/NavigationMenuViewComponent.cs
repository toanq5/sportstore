﻿using Microsoft.AspNetCore.Mvc;
using SportsStore.Repository;
using System.Linq;

namespace SportsStore.Components
{
    public class NavigationMenuViewComponent : ViewComponent
    {
        private IProductRepository repository;

        public NavigationMenuViewComponent(IProductRepository repo)
        {
            repository = repo;
        }

        public IViewComponentResult Invoke()
        {
            return View(repository.Products.Select(x => x.Category).Distinct().OrderBy(x => x));
        }
    }
}