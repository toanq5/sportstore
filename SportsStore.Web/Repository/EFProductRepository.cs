﻿using SportsStore.Dal;
using SportsStore.Models;
using System.Linq;

namespace SportsStore.Repository
{
    public class EFProductRepository : IProductRepository
    {
        private readonly ApplicationDbContext context;

        public EFProductRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public IQueryable<Product> Products => context.Products;
    }
}