﻿using SportsStore.Models;
using System.Linq;

namespace SportsStore.Repository
{
    public interface IProductRepository
    {
        IQueryable<Product> Products { get; }
    }
}