﻿using Microsoft.EntityFrameworkCore;
using SportsStore.Models;

namespace SportsStore.Dal
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Product> Products { get; set; }
    }
}