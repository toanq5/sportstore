﻿using Moq;
using SportsStore.Controllers;
using SportsStore.Models;
using SportsStore.Paging;
using SportsStore.Repository;
using System.Linq;
using Xunit;

namespace SportsStore.Test
{
    public class ProductControllerTests
    {
        [Fact]
        public void Can_Paginate()
        {
            //Arrange
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns((new Product[] {
                    new Product {ProductID = 1,Category="Soccer", Name = "P1"},
                    new Product {ProductID = 2, Name = "P2"},
                    new Product {ProductID = 3, Name = "P3"},
                    new Product {ProductID = 4, Name = "P4"},
                    new Product {ProductID = 5, Name = "P5"}
                }).AsQueryable());
            ProductController productController = new ProductController(mock.Object)
            {
                PageSize = 3
            };
            //Act
            ProductsListViewModel result = productController.List("Soccer", 1).ViewData.Model as ProductsListViewModel;
            var prodArray = result.Products.ToArray();
            var first = prodArray.Where(p => p.ProductID == 1).FirstOrDefault();
            //Asset
            PagingInfo pagingInfo = result.PagingInfo;
            Assert.Equal("Soccer", first.Category);
            Assert.Equal(2, pagingInfo.CurrentPage);
            Assert.Equal(3, pagingInfo.ItemsPerPage);
            Assert.Equal(5, pagingInfo.TotalItems);
            Assert.Equal(2, pagingInfo.TotalPages);
        }
    }
}